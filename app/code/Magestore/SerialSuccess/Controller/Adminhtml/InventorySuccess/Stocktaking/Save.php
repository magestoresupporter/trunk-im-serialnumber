<?php

/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\SerialSuccess\Controller\Adminhtml\InventorySuccess\Stocktaking;

use Magestore\InventorySuccess\Api\Data\Stocktaking\StocktakingInterface;

/**
 * Class Save
 * @package Magestore\InventorySuccess\Controller\Adminhtml\Stocktaking
 */
class Save extends \Magestore\InventorySuccess\Controller\Adminhtml\Stocktaking\Save
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data = $this->getRequest()->getPostValue()) {
            try {
                $stocktaking = $this->stocktakingFactory->create();
                $stocktakingData = $this->getStocktakingData($data);
                if (isset($data[StocktakingInterface::STOCKTAKING_ID])) {
                    $stocktaking->setId($data[StocktakingInterface::STOCKTAKING_ID]);
                }
                $stocktakingData['products'] = [];
                if (isset($data['links'])) {
                    $stocktakingData['products'] = $this->getProducts($data['links']);
                }
                if (count($stocktakingData['products']) <= 0) {
                    if ($this->getRequest()->getParam('back') == 'confirm' ||
                        $this->getRequest()->getParam('back') == 'verify'
                    ) {
                        $this->messageManager->addErrorMessage(__('No product to stocktake.'));
                        return $resultRedirect->setPath('*/*/edit', ['id' => $stocktaking->getId()]);
                    }
                }
                $this->stocktakingManagement->createStocktaking($stocktaking, $stocktakingData);
                /* if created stocktaking then complete it */
                if ($stocktaking->getId()) {
                    if ($this->getRequest()->getParam('back') == 'cancel') {
                        $this->saveSerialNumber($data, $stocktaking->getId());
                        $this->messageManager->addSuccessMessage(__('The stocktaking has been canceled.'));
                        return $resultRedirect->setPath('*/*/cancel', ['id' => $stocktaking->getId()]);
                    }
                    if ($this->getRequest()->getParam('back') == 'reopen') {
                        $this->saveSerialNumber($data, $stocktaking->getId());
                        $this->messageManager->addSuccessMessage(__('The stocktaking has been re-open.'));
                        return $resultRedirect->setPath('*/*/reopen', ['id' => $stocktaking->getId()]);
                    }
                    if ($this->getRequest()->getParam('back') == 'delete') {
                        $this->messageManager->addSuccessMessage(__('The stocktaking has been deleted.'));
                        return $resultRedirect->setPath('*/*/delete', ['id' => $stocktaking->getId()]);
                    }

                    if ($this->getRequest()->getParam('back') == 'confirm') {
                        $this->saveSerialNumber($data, $stocktaking->getId());
                        $this->messageManager->addSuccessMessage(__('The stocktaking has been completed.'));
                        return $resultRedirect->setPath('*/*/edit', ['id' => $stocktaking->getId()]);
                    }
                    $this->messageManager->addSuccessMessage(__('The stocktaking has been saved.'));

                    if ($this->getRequest()->getParam('back') == 'new') {
                        return $resultRedirect->setPath('*/*/new');
                    }

                    if ($this->getRequest()->getParam('back') != 'close') {
                        $this->saveSerialNumber($data, $stocktaking->getId());
                        return $resultRedirect->setPath('*/*/edit', ['id' => $stocktaking->getId()]);
                    }
                }
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back') == 'new') {
                    return $resultRedirect->setPath('*/*/new');
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // $this->messageManager->addErrorMessage($e->getMessage());
                $this->messageManager->addErrorMessage(__('Stocktaking code was existed.'));
                $this->_getSession()->setFormData($data);
                if (isset($data['stocktaking_id'])) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $data['stocktaking_id']]);
                }
                return $resultRedirect->setPath('*/*/new');
            }
        }
        $this->messageManager->addErrorMessage(
            __('Unable to find stocktaking stock to create')
        );
        return $resultRedirect->setPath('*/*/');
    }

    public function saveSerialNumber($data, $stocktakeId){
        $serialSuccessEnabled = $this->_objectManager->create('Magento\Framework\Module\Manager')
            ->isEnabled('Magestore_SerialSuccess');
        if ($serialSuccessEnabled) {
            $serialItemFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ItemFactory');
            $serialTransFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\TransactionFactory');
            $collectionFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory');
            $duplicate = 0;

            $desc = json_encode(["taking_stock" => $stocktakeId]);
            $serialProductsData = $data['links']['product_list'];
            foreach ($serialProductsData as &$p) {
                if($p['serial-data']){
                    $serials = $collectionFactory->create()
                        ->addFieldToSelect('serial')
                        ->addFieldToFilter('product_id', ['eq' => $p['id']])
                        ->getColumnValues('serial');
//                    $serials = array_diff($p['serial-data'], $serials);

                    foreach ($p['serial-data'] as $serial) {
                        if(!in_array($serial, $serials)){
                            $item = $serialItemFactory->create();
                            $item->setData('product_id', $p['id']);
                            $item->setData('serial', $serial);
                            $item->setData('created_at', time());
                            $item->setData('status', 1);
                            $item->save();
                        }else{
                            $item = $collectionFactory->create()
                                ->addFieldToFilter('serial', ['eq' => $serial])
                                ->getFirstItem();
                        }

                        $trans = $serialTransFactory->create();
                        $trans->setData('serial_item_id', $item->getId());
                        $trans->setData('status', 1);
                        $trans->setData('created_at', time());
                        $trans->setData('desc', $desc);
                        $trans->save();
                    }
                }
            }
            if ($duplicate != 0)
                $this->messageManager->addNoticeMessage(
                    __('Warning! We have found ' . $duplicate . ' duplicate serials and ignored them, you can manually add them later')
                );
        }
    }

    /**
     * get products to stocktaking stock
     *
     * @param array
     * @return array
     */
    public function getProducts($dataLinks)
    {
        $products = [];
        if (isset($dataLinks['product_list'])) {
            foreach ($dataLinks['product_list'] as $product) {
                $products[$product['id']] = ['product_sku' => $product['sku'],
                    'product_name' => $product['name'],
                    'old_qty' => $product['total_qty'],
                    'stocktaking_qty' => $product['stocktaking_qty'],
                    'stocktaking_reason' => isset($product['stocktaking_reason']) ? $product['stocktaking_reason'] : '',
                ];
                if(isset($product['serial_string']) && $product['serial_string']){
                    $products[$product['id']]['serial_string'] = $product['serial_string'];
                }
            }
        }
        return $products;
    }

}
