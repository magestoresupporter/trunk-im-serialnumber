<?php

/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\SerialSuccess\Controller\Adminhtml\InventorySuccess\AdjustStock;

use Magestore\InventorySuccess\Api\Data\AdjustStock\AdjustStockInterface;

/**
 * Class Save
 * @package Magestore\InventorySuccess\Controller\Adminhtml\AdjustStock
 */
class Save extends \Magestore\InventorySuccess\Controller\Adminhtml\AdjustStock\Save
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data = $this->getRequest()->getPostValue()) {
            try {
                $adjustStock = $this->adjustStockFactory->create();
                $adjustData = $this->getAdjustData($data);
                if (isset($data[AdjustStockInterface::ADJUSTSTOCK_ID]))
                    $adjustStock->setId($data[AdjustStockInterface::ADJUSTSTOCK_ID]);
                $adjustData['products'] = [];
                if (isset($data['links'])) {
                    $adjustData['products'] = $this->getProducts($data['links']);
                }
                $this->adjustStockManagement->createAdjustment($adjustStock, $adjustData);
                /* if created adjuststock then complete it */
                if ($adjustStock->getId()) {
                    if ($this->getRequest()->getParam('back') == 'confirm') {
                        if (count($adjustData['products']) <= 0) {
                            $this->messageManager->addErrorMessage(__('No product to adjust stock.'));
                            return $resultRedirect->setPath('*/*/edit', ['id' => $adjustStock->getId()]);
                        }
                        $this->adjustStockManagement->complete($adjustStock);

                        $this->saveSerialNumber($data, $adjustStock->getId());

                        $this->messageManager->addSuccessMessage(__('The adjustment has been confirmed.'));
                        return $resultRedirect->setPath('*/*/edit', ['id' => $adjustStock->getId()]);
                    }
                    $this->messageManager->addSuccessMessage(__('The adjustment has been saved.'));
                    if ($this->getRequest()->getParam('back') == 'edit') {
                        return $resultRedirect->setPath('*/*/edit', ['id' => $adjustStock->getId()]);
                    }
                }

                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back') == 'new') {
                    return $resultRedirect->setPath('*/*/new');
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Adjustment code was existed.'));
                $this->_getSession()->setFormData($data);
                if (isset($data['adjuststock_id'])) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $data['adjuststock_id']]);
                }
                return $resultRedirect->setPath('*/*/new');
            }
        }
        $this->messageManager->addErrorMessage(
            __('Unable to find adjust stock to create')
        );
        return $resultRedirect->setPath('*/*/');
    }

    public function saveSerialNumber($data, $adjustId){
        $serialSuccessEnabled = $this->_objectManager->create('Magento\Framework\Module\Manager')
            ->isEnabled('Magestore_SerialSuccess');
        if ($serialSuccessEnabled) {
            $serialItemFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ItemFactory');
            $serialTransFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\TransactionFactory');
            $collectionFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory');
            $duplicate = 0;

            $desc = json_encode(["adjustment_stock" => $adjustId]);
            $serialProductsData = $data['links']['product_list'];
            foreach ($serialProductsData as &$p) {
                if($p['serial-data']){
                    $serials = $collectionFactory->create()
                        ->addFieldToSelect('serial')
                        ->addFieldToFilter('product_id', ['eq' => $p['id']])
                        ->getColumnValues('serial');

//                    $serials = array_diff($p['serial-data'], $serials);

                    foreach ($p['serial-data'] as $serial) {
                        if(!in_array($serial, $serials)){
                            $item = $serialItemFactory->create();
                            $item->setData('product_id', $p['id']);
                            $item->setData('serial', $serial);
                            $item->setData('created_at', time());
                            $item->setData('status', 1);
                            $item->save();
                        }else{
                            $item = $collectionFactory->create()
                                ->addFieldToFilter('serial', ['eq' => $serial])
                                ->getFirstItem();
                        }

                        $trans = $serialTransFactory->create();
                        $trans->setData('serial_item_id', $item->getId());
                        $trans->setData('status', 1);
                        $trans->setData('created_at', time());
                        $trans->setData('desc', $desc);
                        $trans->save();
                    }
                }
            }
            if ($duplicate != 0)
                $this->messageManager->addNoticeMessage(
                    __('Warning! We have found ' . $duplicate . ' duplicate serials and ignored them, you can manually add them later')
                );
        }
    }

    /**
     * get products to adjust stock
     *
     * @param array
     * @return array
     */
    public function getProducts($dataLinks)
    {
        $products = [];
        if (isset($dataLinks['product_list'])) {
            if ($this->helper->getAdjustStockChange()) {
                foreach ($dataLinks['product_list'] as $product) {
                    $adjustQty = $product['change_qty'] + $product['total_qty'];
                    $products[$product['id']] = [
                        'adjust_qty' => $adjustQty,
                        'product_sku' => $product['sku'],
                        'product_name' => $product['name'],
                        'old_qty' => $product['total_qty'],
                        'change_qty' => $product['change_qty']
                    ];
                    if(isset($product['serial_string']) && $product['serial_string']){
                        $products[$product['id']]['serial_string'] = $product['serial_string'];
                    }
                }
            } else {
                foreach ($dataLinks['product_list'] as $product) {
                    $changeQty = $product['adjust_qty'] - $product['total_qty'];
                    $products[$product['id']] = [
                        'adjust_qty' => $product['adjust_qty'],
                        'product_sku' => $product['sku'],
                        'product_name' => $product['name'],
                        'old_qty' => $product['total_qty'],
                        'change_qty' => $changeQty
                    ];
                    if(isset($product['serial_string']) && $product['serial_string']){
                        $products[$product['id']]['serial_string'] = $product['serial_string'];
                    }
                }
            }
        }
        return $products;
    }
}
