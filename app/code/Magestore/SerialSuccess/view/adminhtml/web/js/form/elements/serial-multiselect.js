/*
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_Ui/js/form/element/multiselect',
    'mageUtils',
    'underscore'
], function ($, MultiSelect, utils,_) {
    'use strict';

    /**
     * Recursively set to object item like value and item.value like key.
     *
     * @param {Array} data
     * @param {Object} result
     * @returns {Object}
     */
    function indexOptions(data, result) {
        var value;

        result = result || {};

        data.forEach(function (item) {
            value = item.value;

            if (Array.isArray(value)) {
                indexOptions(value, result);
            } else {
                result[value] = item;
            }
        });

        return result;
    }

    return MultiSelect.extend({
        defaults: {
            size: 5,
            elementTmpl: 'Magestore_SerialSuccess/form/elements/multiselect',
            listens: {
                value: 'setDifferedFromDefault setPrepareToSendData'
            }
        },

        /**
         * Sets the prepared data to dataSource
         * by path, where key is component link to dataSource with
         * suffix "-prepared-for-send"
         *
         * @param {Array} data - current component value
         */
        setPrepareToSendData: function (data) {
            if (_.isUndefined(data) || !data.length) {
                data = '';
            }
            if(data && typeof data == 'string'){
                var dataOptions = [];
                var dataArray = data.split(",");
                dataArray.forEach(function (value) {
                    var option = [];
                    option['value'] = value;
                    option['label'] = value;
                    dataOptions.push(option);
                });
                this.indexedOptions = indexOptions(dataOptions);
                this.options(dataOptions);
            }
            this.source.set(this.dataScope + '-prepared-for-send', data);
        },

        /**
         * @inheritdoc
         */
        hasChanged: function () {
            var value = this.value(),
                initial = this.initialValue;

            return !utils.equalArrays(value, initial);
        },

        selectChange: function (obj,e) {
            var qty = obj.value().length;
            var elmName = e.target.name;
            var index = '';
            if(elmName.search('receiving') > 0){
                index= elmName.replace('links[receiving_products][','');
            }
            else if(elmName.search('return') > 0){
                index= elmName.replace('links[return_products][','');
            }
            else{
                index= elmName.replace('links[shortfall_products][','');
            }
            index = index.replace('][serial_shortfall]','');
            var qtyReceiveInputName = 'links[receiving_products]['+index+'][qty]';
            var qtyReturnInputName = 'links[return_products]['+index+'][qty]';
            var qtyShortfallInputName = 'links[shortfall_products]['+index+'][qty]';
            $("input[name='"+qtyReceiveInputName+"']").val(qty);
            $("input[name='"+qtyReceiveInputName+"']").change();
            $("input[name='"+qtyReturnInputName+"']").val(qty);
            $("input[name='"+qtyReturnInputName+"']").change();
            $("input[name='"+qtyShortfallInputName+"']").val(qty);
            $("input[name='"+qtyShortfallInputName+"']").change();
        },

        applySelectedAfterRender: function (elm, obj) {
            var qty = obj.value().length;
            var elmName = elm.name;
            var index = '';
            if(elmName.search('receiving') > 0){
                index= elmName.replace('links[receiving_products][','');
            }
            else if(elmName.search('return') > 0){
                index= elmName.replace('links[return_products][','');
            }
            else{
                index= elmName.replace('links[shortfall_products][','');
            }
            index = index.replace('][serial_shortfall]','');
            var qtyReceiveInputName = 'links[receiving_products]['+index+'][qty]';
            var qtyReturnInputName = 'links[return_products]['+index+'][qty]';
            var qtyShortfallInputName = 'links[shortfall_products]['+index+'][qty]';
            $("input[name='"+qtyReceiveInputName+"']").val(qty);
            $("input[name='"+qtyReceiveInputName+"']").change();
            $("input[name='"+qtyReturnInputName+"']").val(qty);
            $("input[name='"+qtyReturnInputName+"']").change();
            $("input[name='"+qtyShortfallInputName+"']").val(qty);
            $("input[name='"+qtyShortfallInputName+"']").change();
        }
    });
});
