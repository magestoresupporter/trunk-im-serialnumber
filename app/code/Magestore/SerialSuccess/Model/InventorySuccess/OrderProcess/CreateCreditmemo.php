<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\SerialSuccess\Model\InventorySuccess\OrderProcess;

class CreateCreditmemo extends \Magestore\InventorySuccess\Model\OrderProcess\CreateCreditmemo
{
    /**
     * execute the process
     * 
     * @param \Magento\Sales\Model\Order\Creditmemo\Item $item
     * @return bool
     */
    public function execute($item)
    {
        if(!$this->canProcessItem($item)){
            return;
        }

        // Save serial_string to memo_item and save serial_refund
        $this->saveSerialString($item);

        $this->processRefundItem($item);
        
        $this->markItemProcessed($item);
        
        return true;              
    }

    /**
     * Abel edit
     * Save serial_string to memo_item
     *
     * @param \Magento\Sales\Model\Order\Creditmemo\Item $item
     */
    public function saveSerialString($item)
    {
        $orderItem = $item->getOrderItem();
        if($item->getData('serial_string') == ''
            || $item->getData('serial_string') == null) {
            $data = $this->request->getParam('creditmemo');
            $order_item_id = $item->getData('order_item_id');
            $serialString = '';
            if(isset($data['items'][$order_item_id]['itemsserial'])) {
                $serialString = implode(',', $data['items'][$order_item_id]['itemsserial']);
                // check back_to_stock
                if(isset($data['items'][$order_item_id]['back_to_stock'])
                    && (int)$data['items'][$order_item_id]['back_to_stock']) {
                    $this->serialBackToStock($data['items'][$order_item_id]['itemsserial'], $item->getData('parent_id'));
                }
                $item->setData('serial_string', $serialString)->save();

                // save serial_refund to order_item
                $curItemSerialRefund = $orderItem->getData('serial_refund');
                if($curItemSerialRefund){
                    $itemSerialRefund = $curItemSerialRefund.",".$serialString;
                }else{
                    $itemSerialRefund = $serialString;
                }
                $orderItem->setData('serial_refund', $itemSerialRefund)->save();
            }
        }
    }

    /**
     * enable serial back to stock
     *
     * @param array $serial
     * @param string $memoId
     */
    public function serialBackToStock($serial, $memoId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $serialFactory = $objectManager->get('Magestore\SerialSuccess\Model\ItemFactory');
        $transactionFactory = $objectManager->get('Magestore\SerialSuccess\Model\TransactionFactory');
        $curDate = $objectManager->get('Magento\Framework\Stdlib\DateTime\DateTime')->gmtDate();
        foreach ($serial as $item) {
            $serialModel = $serialFactory->create()->load($item, 'serial');
            if($serialModel->getId()) {
                $serialModel->setData('status', 1)->save();

                // create transaction
                $transaction = $transactionFactory->create();
                $data = [
                    'serial_item_id' => $serialModel->getId(),
                    'created_at' => $curDate,
                    'status' => 1,
                    'desc' => json_encode(['refund_id' => $memoId])
                ];
                $transaction->setData($data)->save();
            }
        }
    }

}
