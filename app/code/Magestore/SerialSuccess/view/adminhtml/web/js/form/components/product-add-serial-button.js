/*
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magestore_SerialSuccess/js/form/elements/import-button'
], function ($, Component, ko) {
    'use strict';

    var self = this;
    return Component.extend({
        defaults: {
            links: {
                value: '${ $.provider }:${ $.dataScope }'
            }
        },

        /** @inheritdoc */
        initObservable: function () {
            return this._super()
                .observe([
                    'visible',
                    'disabled',
                    'title',
                    'serialData'
                ]);
        },

        handleOnclick: function (data, event) {
            $('#add-serial-modal').modal('openModal');
        }
    });
});
