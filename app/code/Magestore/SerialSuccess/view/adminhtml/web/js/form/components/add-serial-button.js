/*
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magestore_SerialSuccess/js/form/elements/import-button'
], function ($, Component, ko) {
    'use strict';

    var self = this;
    return Component.extend({
        defaults: {
            links: {
                value: '${ $.provider }:${ $.dataScope }',
            },
            serialData: ''
        },

        /** @inheritdoc */
        initObservable: function () {
            return this._super()
                .observe([
                    'visible',
                    'disabled',
                    'title',
                    'serialData'
                ]);
        },

        handleOnclick: function (data, event) {
            var elm = event.target;
            var trElm = $(elm).parent().parent().parent();
            var qtyInput = $(trElm).find('.serial_num_qty').find('input');
            window.curQtyInputElm = qtyInput;
            if (event.target.name !== '') {
                window.removeAllSerial();
                var productId = event.target.name;
                $('#product-id').html(productId);
                if (typeof window.serials[productId] === 'undefined'){
                    window.serials[productId] = [];
                    if(window['old_serial_string_'+productId]){
                        var serials = window['old_serial_string_'+productId].split(",");
                        serials.forEach(function (item) {
                            window.serials[productId].push(item);
                        });
                    }
                }

                window.serials[productId].forEach(function (e) {
                    window.addSerial(e);
                });
                window.titleUpdater = this.title;
                $('#add-serial-modal').modal('openModal');
            }
        }
    });
});
