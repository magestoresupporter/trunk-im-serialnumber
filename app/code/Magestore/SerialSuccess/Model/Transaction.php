<?php
namespace Magestore\SerialSuccess\Model;

use Magento\Framework\Model\Context;

class Transaction extends \Magento\Framework\Model\AbstractModel {
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        \Magestore\SerialSuccess\Model\ResourceModel\Transaction $resource = null,
        \Magestore\SerialSuccess\Model\ResourceModel\Transaction\Collection $resourceCollection)
    {
        parent::__construct($context, $registry, $resource, $resourceCollection);
    }
}