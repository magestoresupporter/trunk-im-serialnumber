<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\SerialSuccess\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddBarcodeAfterSerialSave implements ObserverInterface
{
    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_url;
    protected $_request;

    public function __construct(
        \Magento\Backend\Model\UrlInterface $urlInterface,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->_url     = $urlInterface;
        $this->_request = $request;
    }

    /**
     * @param EventObserver $observer
     * @return EventObserver
     */
    public function execute(EventObserver $observer)
    {
        $serial = $observer->getEvent()->getSerial();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $moduleManager = $objectManager->get('Magento\Framework\Module\Manager');
        $isBarcodeEnable = $moduleManager->isEnabled('Magestore_BarcodeSuccess') ? true : false;
        if($isBarcodeEnable){
            $productId = $serial->getProductId();
            $serialNumber = $serial->getSerial();
            $barcodeCollection = $objectManager->create('Magestore\BarcodeSuccess\Model\Barcode')->getCollection()
                ->addFieldToFilter('barcode', $serialNumber)
                ->addFieldToFilter('product_id', $productId);
            if(!$barcodeCollection->getSize()){
                $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
                $newBarcode = [
                    'barcode' => $serialNumber,
                    'product_id' => $productId,
                    'product_sku' => $product->getSku(),
                    'supplier_id' => 0,
                    'purchase_id' => 0,
                    'qty'   => 1
                ];
                $barcodeModel = $objectManager->create('Magestore\BarcodeSuccess\Model\Barcode');
                $barcodeModel->setData($newBarcode);
                $barcodeModel->save();
            }
        }
        return;
    }
}