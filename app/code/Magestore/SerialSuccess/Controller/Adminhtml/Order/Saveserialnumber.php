<?php
namespace Magestore\SerialSuccess\Controller\Adminhtml\Order;

class Saveserialnumber extends \Magestore\SerialSuccess\Controller\Adminhtml\AbstractAction {
    public function execute()
    {
        $orderItemId = $this->getRequest()->getParam('itemId');
        $listSerial = $this->getRequest()->getParam('data');

        $orderItem = $this->_objectManager->get('Magento\Sales\Model\Order\ItemFactory')
            ->create()->load($orderItemId);
        if($orderItem->getId()) {
            $productId = '';
            if($orderItem->getData('product_type') == 'simple') {
                $productId = $orderItem->getData('product_id');
            } elseif ($orderItem->getData('product_type') == 'configurable') {
                $childItem = $this->_objectManager->get('Magento\Sales\Model\Order\ItemFactory')
                    ->create()->load($orderItem->getId(), 'parent_item_id');
                $productId = $childItem->getData('product_id');
            }
            $curSerialNumber = $this->_itemFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $productId)
                ->getColumnValues('serial');

            $curOrderItemSerials = [];
            $curOrderItemSerialString = $orderItem->getData('serial_string');
            if($curOrderItemSerialString){
                $curOrderItemSerials = explode(",",$orderItem->getData('serial_string'));
            }

//            $correctSerial = array_intersect($curSerialNumber, $listSerial);
            $serialString = implode(",", $listSerial);
            foreach ($listSerial as $serial) {
                if(!in_array($serial, $curSerialNumber)){
                    $serialItem = $this->_itemFactory->create();
                    $serialItem->setData('product_id', $productId);
                    $serialItem->setData('serial', $serial);
                    $serialItem->setData('created_at', time());
                    $serialItem->setData('status', 2);
                    $serialItem->save();
                }else{
                    $serialItem = $this->_itemFactory->create()->getCollection()
                        ->addFieldToFilter('serial', ['eq' => $serial])
                        ->getFirstItem();
                }
                if($serialItem->getId()) {
                    $serialItem->setData('status', 2)->save();
                }

                if(!in_array($serial,$curOrderItemSerials)){
                    $desc = json_encode(["sales_order_item" => $orderItem->getId()]);
                    $trans = $this->_transactionFactory->create();
                    $trans->setData('serial_item_id', $serialItem->getId());
                    $trans->setData('status', 2);
                    $trans->setData('created_at', time());
                    $trans->setData('desc', $desc);
                    $trans->save();
                }
            }

            $orderItem->setData('serial_string', $serialString)->save();

            // Save serial_string to sales_order
            $orderModel = $orderItem->getOrder();
            $curOrderSerialString = $orderModel->getData('serial_string');
            if(!$curOrderSerialString){
                $orderSerialStrings = $listSerial;
                $orderSerialString = implode(",", $orderSerialStrings);

            }
            else{
                $orderSerialStrings = [];
                foreach($orderModel->getAllVisibleItems() as $item){
                    if($item->getData('serial_string'))
                        $orderSerialStrings[] = $item->getData('serial_string');
                }
                $orderSerialString = implode(",", $orderSerialStrings);
            }
            $orderModel->setData('serial_string', $orderSerialString)->save();
        }
    }
}