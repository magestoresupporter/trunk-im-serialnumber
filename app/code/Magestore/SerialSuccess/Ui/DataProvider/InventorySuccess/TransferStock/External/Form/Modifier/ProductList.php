<?php

/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magestore\SerialSuccess\Ui\DataProvider\InventorySuccess\TransferStock\External\Form\Modifier;

use Magento\Ui\Component\Form;

/**
 * Class Related
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductList extends \Magestore\InventorySuccess\Ui\DataProvider\TransferStock\External\Form\Modifier\ProductList
{
    /**
     * Fill data column
     *
     * @param ProductModel
     * @return array
     */
    protected function fillDynamicData($product)
    {
        echo "<script language='javascript' type='text/javascript'>";
        echo "window.old_serial_string_".$product->getProductId()."='".$product->getSerialString()."';";
        echo "</script>";

        return [
            'id' => $product->getProductId(),
            'sku' => $product->getProductSku(),
            'name' => $product->getProductName(),
            'available_qty' => $product->getAvailableQty(),
            'image' => $product->getImageUrl(),
            //'position' => $product->getPosition(),
            'qty' => $product->getQty(),
            'serial_string' => $product->getSerialString()
        ];
    }

    /**
     * Fill meta columns
     *
     * @return array
     */
    protected function fillModifierMeta()
    {
        $moduleManager = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\Module\Manager');
        $showAddSerial = $moduleManager->isEnabled('Magestore_SerialSuccess') ? true : false;
        return [
            'id' => $this->getTextColumn('id', true, __('ID'), 10),
            'sku' => $this->getTextColumn('sku', false, __('SKU'), 20),
            'name' => $this->getTextColumn('name', false, __('Name'), 30),
            'available_qty' => $this->getTextColumn('available_qty', false, __('Available Qty'), 40),
            'qty' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'dataType' => Form\Element\DataType\Number::NAME,
                            'formElement' => Form\Element\Input::NAME,
                            'componentType' => Form\Field::NAME,
                            'dataScope' => 'qty',
                            'label' => __('Qty'),
                            'fit' => true,
                            'additionalClasses' => 'admin__field-small serial_num_qty',
                            'sortOrder' => 50,
                            'validation' => [
                                'validate-number' => true,
                                'validate-greater-than-zero' => true,
                                'required-entry' => true
                            ],
                        ],
                    ],
                ],
            ],
            'image' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'componentType' => Form\Field::NAME,
                            'formElement' => Form\Element\Input::NAME,
//                            'elementTmpl' => 'ui/dynamic-rows/cells/thumbnail',
                            'elementTmpl' => 'Magestore_InventorySuccess/dynamic-rows/cells/thumbnail',
                            'dataType' => Form\Element\DataType\Media::NAME,
                            'dataScope' => 'image',
                            'fit' => __('Thumbnail'),
                            'label' => __('Thumbnail'),
                            'sortOrder' => 55,
                            'visible' => $this->getVisibleImage()
                        ],
                    ],
                ],
            ],
            'serial_string' => $this->getTextColumn('serial_string', false, __('Serial Number'), 57),
            'add_serial_button' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Action'),
                            'formElement' => 'container',
                            'componentType' => 'container',
                            'component' => 'Magestore_SerialSuccess/js/form/components/add-serial-button',
                            'actions' => [],
                            'title' => __('Add serial, 0 added'),
                            'dataScope' => 'id',
                            'sampleValue' => 'ahihi',
                            'provider' => null,
                            'visible' => $showAddSerial
                        ],
                    ],
                ],
            ],
            'actionDelete' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'additionalClasses' => 'data-grid-actions-cell',
                            'componentType' => 'actionDelete',
                            'dataType' => Form\Element\DataType\Text::NAME,
                            'label' => __('Actions'),
                            'sortOrder' => 60,
                            'fit' => true,
                        ],
                    ],
                ],
            ],
            'position' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'dataType' => Form\Element\DataType\Number::NAME,
                            'formElement' => Form\Element\Input::NAME,
                            'componentType' => Form\Field::NAME,
                            'dataScope' => 'position',
                            'sortOrder' => 70,
                            'visible' => false,
                        ],
                    ],
                ],
            ],
        ];
    }

}
