<?php
namespace Magestore\SerialSuccess\Model\ResourceModel\Item;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
    public function _construct() {
        parent::_construct();
        $this->_init('Magestore\SerialSuccess\Model\Item','Magestore\SerialSuccess\Model\ResourceModel\Item');
    }
}