/*
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_Ui/js/form/provider',
    'Magento_Ui/js/modal/alert'
], function ($, Element, alert) {
    'use strict';

    return Element.extend({
        /**
         * Saves currently available data.
         *
         * @param {Object} [options] - Addtitional request options.
         * @returns {Provider} Chainable.
         */
        save: function (options) {
            var data = this.get('data');

            if (typeof window.serials !== 'undefined') {
                if(typeof data.links !== 'undefined'){
                    data.links.external_products.forEach(function (e) {
                        e['serial_string'] = (window.serials[e['id']])?window.serials[e['id']].join():"";
                        e['serial-data'] = window.serials[e['id']];
                    });
                    delete window.serials;
                }
            }

            this.client.save(data, options);

            return this;
        },
    });
});
