<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\SerialSuccess\Model\InventorySuccess\ResourceModel\TransferStock;

use Magestore\InventorySuccess\Api\Db\QueryProcessorInterface;

class TransferStockProduct extends \Magestore\InventorySuccess\Model\ResourceModel\TransferStock\TransferStockProduct
{
    public function updateTransferstockSerialShortfall($transferstock_id, $activity_products)
    {
        /* start queries processing */
        $this->_queryProcessor->start();

        $productIds = [];
        $serialStrings = [];
        foreach($activity_products as $product){
            $productIds[] = $product['id'];
            foreach($product['serial_shortfall'] as $serial){
                $serialStrings[$product['id']][] = $serial;
            }
        }

        $transferProductCollection = $this->_transferStockProductCollectionFactory->create();
        $products = $transferProductCollection->addFieldToFilter('transferstock_id', $transferstock_id);
        if (count($productIds)) {
            $products->addFieldToFilter('product_id', ['in' => $productIds]);
        }

        $connection = $this->getConnection();
        $newSerialShortfalls = [];

        if ($products->getSize()) {
            $conditions = [];
            foreach ($products as $product) {
                $oldSerialShortfall = $product->getSerialShortfall();
                $newSerialShortfalls[$product->getProductId()] = implode(",",array_diff(explode(",",$oldSerialShortfall),$serialStrings[$product->getProductId()]));

                $case = $connection->quoteInto('?', $product->getProductId());

                $condition = $connection->quoteInto("?", $newSerialShortfalls[$product->getProductId()]);
                $conditions[$case] = $condition;
            }

            $value = $connection->getCaseSql('product_id', $conditions, 'serial_shortfall');
            $where = [
                'product_id IN (?)' => array_keys($newSerialShortfalls),
                'transferstock_id=?' => $transferstock_id
            ];
            /* add query to the processor */
            $this->_queryProcessor->addQuery(['type' => QueryProcessorInterface::QUERY_TYPE_UPDATE,
                'values' =>  ['serial_shortfall' => $value],
                'condition' => $where,
                'table' => $this->getTable('os_transferstock_product')
            ]);
        }

        /* process queries in Processor */
        $this->_queryProcessor->process();

        return $this;
    }
}