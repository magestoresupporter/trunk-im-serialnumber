<?php
namespace Magestore\SerialSuccess\Block\Adminhtml\Serial;

/**
 * Class AbstractGridProduct
 * @package Magestore\InventorySuccess\Block\Adminhtml\ManageStock
 */
class AbstractSerialGrid extends \Magento\Backend\Block\Widget\Grid\Extended {
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    protected $_status;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var string
     */
    protected $_hiddenInputField = 'selected_products';

    /** @var \Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory  */
    protected $_serialCollectionFactory;

    /**
     * AbstractGridProduct constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Catalog\Model\Product\Attribute\Source\Status $status
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory $serialCollectionFactory
     * @param array $data
     * @internal param \Magestore\InventorySuccess\Model\ResourceModel\Warehouse\Stock\CollectionFactory $stockCollectionFactory
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $status,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory $serialCollectionFactory,
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        $this->_status = $status;
        $this->jsonEncoder = $jsonEncoder;
        $this->messageManager = $messageManager;
        $this->_serialCollectionFactory = $serialCollectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId("product_serials_list");
        $this->setDefaultSort("asc");
        $this->setUseAjax(true);
    }

    /**
     * Set hidden input field name for selected products
     *
     * @param $name
     */
    protected function _setHiddenInputField($name){
        $this->_hiddenInputField = $name;
    }

    /**
     * get hidden input field name for selected products
     *
     * @return string
     */
    public function getHiddenInputField(){
        return $this->_hiddenInputField;
    }

    /**
     * Prepare collection for grid product
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->getDataColllection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Get Collection for grid product
     *
     * @return Collection
     */
    public function getDataColllection(){
        $collection = $this->_serialCollectionFactory->create();
        $collection = $this->modifyCollection($collection);
        $sort = $this->getRequest()->getParam('sort');
        $dir = $this->getRequest()->getParam('dir');
//        if (array_key_exists($sort, Collection::MAPPING_FIELD)) {
//            $collection->getSelect()->order(Collection::MAPPING_FIELD[$sort]. ' ' . $dir);
//        }
        return $collection;
    }

    /**
     * function to modify collection
     *
     * @param $collection
     * @return $collection
     */
    public function modifyCollection($collection){
        return $collection;
    }

    /**
     * prepare columns for grid product
     *
     * @return $this
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn("id",
            [
                "header" => __("ID"),
                "index" => "serial_item_id",
                'type' => 'number',
                "sortable" => true,
            ]
        );
        $this->addColumn("serial",
            [
                "header" => __("Serial"),
                "index" => "serial",
                'type' => 'text',
                "sortable" => true,
            ]
        );
        $this->addColumn("created_at",
            [
                "header" => __("Time created"),
                "index" => "created_at",
                'type'      => 'datetime',
                "sortable" => true,
            ]
        );
        $this->addColumn("status",
            [
                "header" => __("Status"),
                "index" => "status",
                'type' => 'options',
                "sortable" => true,
                'options' => [
                    1 => __('Enabled'),
                    0 => __('Disabled')
                ]
            ]
        );

        $this->addColumn(
            'edit',
            [
                'header' => __('Edit'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Remove'),
                        'url' => [
                            'base' => 'serialsuccess/serials/remove',
                        ],
                        'field' => 'id',
                        'confirm' => __('Are you sure that you want to remove this serial ?')
                    ],
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action',
            ]
        );

        $this->modifyColumns();
        return parent::_prepareColumns();
    }

    /**
     * function to add, remove or modify product grid columns
     *
     * @return $this
     */
    public function modifyColumns(){
        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl("serialsuccess/serials_product/grid", ["_current" => true]);
    }
}