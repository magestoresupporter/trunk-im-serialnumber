<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\SerialSuccess\Model\InventorySuccess\OrderProcess;

class CreateShipment extends \Magestore\InventorySuccess\Model\OrderProcess\CreateShipment
{
    /**
     * execute the process
     *
     * @param \Magento\Sales\Model\Order\Shipment\Item $item
     * @return bool
     */
    public function execute($item)
    {
        if (!$this->canProcessItem($item)) {
            return;
        }

        // Save serial_string to shipment_item and save serial_shipping to order_item
        $this->saveSerialString($item);

        $this->processShipItem($item);

        $this->markItemProcessed($item);

        return true;
    }

    /**
     * Abel edit
     * Save serial_string to shipment_item and save serial_shipping to order_item
     *
     * @param \Magento\Sales\Model\Order\Shipment\Item $item
     */
    public function saveSerialString($item) {
        $orderItem = $item->getOrderItem();
        if($item->getData('serial_string') == ''
            || $item->getData('serial_string') == null) {
            $data = $this->request->getParam('shipment');
            $dataSerial = $data['itemsserial'];
            $order_item_id = $item->getData('order_item_id');
            $serialString = '';
            if (isset($dataSerial[$order_item_id])) {
                $serialString = implode(',', $dataSerial[$order_item_id]);

                // create transaction
                $this->serialBackToStock($dataSerial[$order_item_id], $item->getData('parent_id'));
            }
            $item->setData('serial_string', $serialString)->save();

            //save serial_shipping for order item
            $curItemSerialShipping = $orderItem->getData('serial_shipping');
            if($curItemSerialShipping){
                $itemSerialShipping = $curItemSerialShipping.",".$serialString;
            }else{
                $itemSerialShipping = $serialString;
            }
            $orderItem->setData('serial_shipping', $itemSerialShipping)->save();

            // save serial_string to shipment
            $shipment = $item->getShipment();
            $curShipmentSerial = $shipment->getData('serial_string');
            if($curShipmentSerial){
                $shipment->setData('serial_string', $curShipmentSerial . ',' . $serialString)->save();
            }else{
                $shipment->setData('serial_string', $serialString)->save();
            }
        }
    }

    /**
     * enable serial back to stock
     *
     * @param array $serial
     * @param string $shippingId
     */
    public function serialBackToStock($serial, $shippingId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $serialFactory = $objectManager->get('Magestore\SerialSuccess\Model\ItemFactory');
        $transactionFactory = $objectManager->get('Magestore\SerialSuccess\Model\TransactionFactory');
        $curDate = $objectManager->get('Magento\Framework\Stdlib\DateTime\DateTime')->gmtDate();
        foreach ($serial as $item) {
            $serialModel = $serialFactory->create()->load($item, 'serial');
            if($serialModel->getId()) {
                // create transaction
                $transaction = $transactionFactory->create();
                $data = [
                    'serial_item_id' => $serialModel->getId(),
                    'created_at' => $curDate,
                    'status' => 2,
                    'desc' => json_encode(['shipping_id' => $shippingId])
                ];
                $transaction->setData($data)->save();
            }
        }
    }

}
