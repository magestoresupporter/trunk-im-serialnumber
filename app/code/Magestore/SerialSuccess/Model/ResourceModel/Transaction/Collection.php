<?php
namespace Magestore\SerialSuccess\Model\ResourceModel\Transaction;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
    public function _construct() {
        parent::_construct();
        $this->_init('Magestore\SerialSuccess\Model\Transaction','Magestore\SerialSuccess\Model\ResourceModel\Transaction');
    }
}