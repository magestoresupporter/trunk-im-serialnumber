<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\SerialSuccess\Controller\Adminhtml\InventorySuccess\TransferStock\Send;

use Magestore\InventorySuccess\Api\Data\TransferStock\TransferActivityInterface;
use Magestore\InventorySuccess\Model\TransferStock as TransferStockModel;
use Magestore\InventorySuccess\Api\Data\TransferStock\TransferStockInterface;
use \Magestore\InventorySuccess\Model\TransferStock\TransferActivity as TransferActivityModel;


class Save extends \Magestore\InventorySuccess\Controller\Adminhtml\TransferStock\Send\Save
{
    /**
     * save deliery product.
     * Steps:
     * + create new deliery
     * + reformat post data of delivery_products
     * + save delivery product into model TransferActivityProduct
     * @param $delivery_products
     */
    public function saveTransferActivityProduct($activity_products, $activity_type)
    {
        $transferActivity = $this->createTransferActivity($activity_products, $activity_type);
        $activityId = $transferActivity->getActivityId();
        if ($activityId) {
            //reformat delivery product data
            $products = [];
            foreach ($activity_products as $item) {
                $product = [];
                $product['activity_id'] = $activityId;
                $product['product_id'] = $item['id'];
                $product['product_name'] = $item['name'];
                $product['product_sku'] = $item['sku'];
                $product['qty'] = $item['qty'];
                if(isset($item['serial_shortfall']) && count($item['serial_shortfall'])>0){
                    $product['serial_string'] = implode(",",$item['serial_shortfall']);
                }
                $products[$item['id']] = $product;
            }

            $serialSuccessEnabled = $this->_objectManager->create('Magento\Framework\Module\Manager')
                ->isEnabled('Magestore_SerialSuccess');
            if ($serialSuccessEnabled) {
                $serialItemFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ItemFactory');
                $serialTransFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\TransactionFactory');
                $collectionFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory');

                foreach ($activity_products as &$p) {
                    if($p['serial_shortfall']){
                        $serials = $collectionFactory->create()
                            ->addFieldToSelect('serial')
                            ->addFieldToFilter('product_id', ['eq' => $p['id']])
                            ->getColumnValues('serial');

                        foreach ($p['serial_shortfall'] as $serial) {
                            if(!in_array($serial, $serials)){
                                $item = $serialItemFactory->create();
                                $item->setData('product_id', $p['id']);
                                $item->setData('serial', $serial);
                                $item->setData('created_at', time());
                                $item->setData('status', 1);
                                $item->save();
                            }else{
                                $item = $collectionFactory->create()
                                    ->addFieldToFilter('serial', ['eq' => $serial])
                                    ->getFirstItem();
                            }
                            if($activity_type == 'receiving'){
                                $desc = json_encode(["receive_stock_activity" => $activityId, "send_stock" => $this->getRequest()->getParam('id')]);
                            }
                            else{
                                $desc = json_encode(["return_stock_activity" => $activityId, "send_stock" => $this->getRequest()->getParam('id')]);
                            }

                            $trans = $serialTransFactory->create();
                            $trans->setData('serial_item_id', $item->getId());
                            $trans->setData('status', 1);
                            $trans->setData('created_at', time());
                            $trans->setData('desc', $desc);
                            $trans->save();
                        }
                    }
                }
            }

            /** @var \Magestore\InventorySuccess\Model\TransferStock\TransferActivityManagement $transferActivityManagement */
            $transferActivityManagement = $this->_objectManager->create('\Magestore\InventorySuccess\Model\TransferStock\TransferActivityManagement');
            $transferActivityManagement->setProducts($transferActivity, $products);
            $transferActivityManagement->updateStock($transferActivity);
            $transferstockId = $this->getRequest()->getParam('id');

            $transferStockProductResoure = $this->_objectManager->create('Magestore\InventorySuccess\Model\ResourceModel\TransferStock\TransferStockProduct');
            $transferStockProductResoure->updateTransferstockSerialShortfall($transferstockId, $activity_products);
            $transferActivityManagement->updateTransferstockProductQtySummary($transferstockId, $activity_products, $activity_type);
        }
    }

    /**
     * save send stock products
     * if admin click on "start request stock" ($update_stock=true) then process update stock
     * @param $data
     * @param $update_stock
     */
    public function saveTransferStockProduct($send_products, $updateStock, $directTransfer)
    {
        if (!$this->validateStockDelivery($send_products)) {
            $this->_coreRegistry->register("send_products", $send_products);
            return false;
        }
        /** @var \Magestore\InventorySuccess\Model\TransferStock\TransferStockManagement $transferStockManagement */
        $transferStockManagement = $this->_objectManager->create('\Magestore\InventorySuccess\Model\TransferStock\TransferStockManagement');
        $sendProductsData = $send_products;
        $data = $this->reformatPostData($send_products);
        $id = $this->getRequest()->getParam('id');
        $transferStock = $this->_objectManager->create('\Magestore\InventorySuccess\Model\TransferStock');
        $transferStock->load($id);

        $transferStockManagement->saveTransferStockProduct($id, $data);

        $serialSuccessEnabled = $this->_objectManager->create('Magento\Framework\Module\Manager')
            ->isEnabled('Magestore_SerialSuccess');
        if ($serialSuccessEnabled) {
            $serialItemFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ItemFactory');
            $serialTransFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\TransactionFactory');
            $collectionFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory');

            foreach ($sendProductsData as &$p) {
                if($p['serial-data']){
                    $serials = $collectionFactory->create()
                        ->addFieldToSelect('serial')
                        ->addFieldToFilter('product_id', ['eq' => $p['id']])
                        ->getColumnValues('serial');

//                    $serials = array_diff($p['serial-data'], $serials);

                    foreach ($p['serial-data'] as $serial) {
                        if(!in_array($serial, $serials)){
                            $item = $serialItemFactory->create();
                            $item->setData('product_id', $p['id']);
                            $item->setData('serial', $serial);
                            $item->setData('created_at', time());
                            $item->setData('status', 1);
                            $item->save();
                        }else{
                            $item = $collectionFactory->create()
                                ->addFieldToFilter('serial', ['eq' => $serial])
                                ->getFirstItem();
                        }

                        $desc = json_encode(["send_stock" => $id]);

                        $trans = $serialTransFactory->create();
                        $trans->setData('serial_item_id', $item->getId());
                        $trans->setData('status', 2);
                        $trans->setData('created_at', time());
                        $trans->setData('desc', $desc);
                        $trans->save();
                    }
                }
            }
        }

        if ($directTransfer) {
            $transferStockManagement->updateStock($transferStock);
            $this->saveTransferActivityProduct($send_products, TransferActivityInterface::ACTIVITY_TYPE_RECEIVING);
            $emailNotification = $this->_emailNotificationFactory->create();
            $emailNotification->notifyCreateDirectTransfer($id);
            return true;
        } elseif ($updateStock) {
            $transferStockManagement->updateStock($transferStock);
            $emailNotification = $this->_emailNotificationFactory->create();
            $emailNotification->notifyCreateNewTransfer($id);
            return true;
        }
        return true;
    }

    public function reformatPostData($data)
    {
        $id = $this->getRequest()->getParam('id');
        $newData = [];

        foreach ($data as $index => $value) {
            if (!isset($value['id']) || !$value['id'] || !isset($value['qty'])) {
                continue;
            }
            $item = [];
            $item['transferstock_id'] = $id;
            $item['product_id'] = $value['id'];
            $item['product_name'] = $value['name'];
            $item['product_sku'] = $value['sku'];
            $item['qty'] = $value['qty'];
            if(isset($value['serial_string']) && $value['serial_string']){
                $item['serial_string'] = $value['serial_string'];
                $item['serial_shortfall'] = $value['serial_string'];
            }

            $newData[$value['id']] = $item;
        }
        return $newData;
    }

}