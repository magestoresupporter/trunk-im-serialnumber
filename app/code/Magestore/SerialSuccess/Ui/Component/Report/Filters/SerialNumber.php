<?php
namespace Magestore\SerialSuccess\Ui\Component\Report\Filters;

class SerialNumber extends \Magento\Ui\Component\Filters\Type\Input {
    /**
     * Apply filter
     *
     * @return void
     */
    protected function applyFilter()
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $serial = $om->get('Magestore\SerialSuccess\Model\Item');
        if (isset($this->filterData[$this->getName()])) {
            $value = $this->filterData[$this->getName()];

            if (!empty($value)) {
                $serial->load($value, 'serial');
                $value = $serial->getData('serial_item_id');
                $filter = $this->filterBuilder->setConditionType('like')
                    ->setField($this->getName())
                    ->setValue(sprintf('%s', $value))
                    ->create();

                $this->getContext()->getDataProvider()->addFilter($filter);
            }
        }
    }
}