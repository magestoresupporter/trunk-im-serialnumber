<?php
namespace Magestore\SerialSuccess\Block\Adminhtml\Product\Edit\Tab\Serial;

class Grid extends \Magestore\SerialSuccess\Block\Adminhtml\Serial\AbstractSerialGrid {
    public function modifyCollection($collection) {
        /** @var \Magestore\SerialSuccess\Model\ResourceModel\Item\Collection $collection */
        $collection->addFieldToFilter(
            'product_id',
            ['eq' => $this->getRequest()->getParam('id')]
        );
        return $collection;
    }
}