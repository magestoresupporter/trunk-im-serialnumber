<?php
namespace Magestore\SerialSuccess\Ui\DataProvider;

class SerialItem extends \Magento\Ui\DataProvider\AbstractDataProvider {
    protected $_request;

    protected $_productId;

    public function __construct (
        $name, $primaryFieldName,
        $requestFieldName,
        \Magestore\SerialSuccess\Model\ItemFactory $itemFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Backend\Model\Session $session,
        array $meta = [],
        array $data = []
    ) {
        $this->_productId = $session->getProductId();
        $this->collection = $itemFactory->create()->getCollection();
        $this->_request = $request;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getCollection() {
        if ($this->_productId)
            $this->collection->addFieldToFilter(
                'product_id',
                ['eq' => $this->_productId]
            );
        return $this->collection;
    }

    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        return $this->getCollection()->toArray();
    }
}
