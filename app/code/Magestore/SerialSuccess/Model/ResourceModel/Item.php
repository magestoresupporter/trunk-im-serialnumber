<?php
namespace Magestore\SerialSuccess\Model\ResourceModel;

class Item extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    public function _construct() {
        $this->_init('os_serial_items', 'serial_item_id');
    }
}