<?php

/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magestore\SerialSuccess\Ui\DataProvider\InventorySuccess\AdjustStock\Form\Modifier;

use Magento\Ui\Component\Form;

/**
 * Class Related
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductList extends \Magestore\InventorySuccess\Ui\DataProvider\AdjustStock\Form\Modifier\ProductList
{
    /**
     * Fill data column
     *
     * @param ProductModel
     * @return array
     */
    protected function fillDynamicData($product)
    {
        echo "<script language='javascript' type='text/javascript'>";
        echo "window.old_serial_string_".$product->getData('product_id')."='".$product->getData('serial_string')."';";
        echo "</script>";

        return [
            'id' => $product->getData('product_id'),
            'sku' => $product->getData('product_sku'),
            'name' => $product->getData('product_name'),
            'total_qty' => $product->getData('old_qty') * 1,
            'change_qty' => $product->getData('change_qty') * 1,
            'adjust_qty' => $product->getData('adjust_qty') * 1,
            'image' => $product->getData('image_url'),
            'serial_string' => $product->getData('serial_string'),
        ];
    }

    /**
     * Fill meta columns
     *
     * @return array
     */
    protected function fillModifierMeta()
    {

        $moduleManager = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\Framework\Module\Manager');
        $showAddSerial = $moduleManager->isEnabled('Magestore_SerialSuccess') ? true : false;

        $additionalColumns = $this->getAdditionalColumns();
        $modifierColumns = array_replace_recursive(
            [
                'id' => $this->getTextColumn('id', true, __('ID'), 10),
                'name' => $this->getTextColumn('name', false, __('Name'), 20),
                'sku' => $this->getTextColumn('sku', false, __('SKU'), 30),
                'total_qty' => $this->getTextColumn('total_qty', false, __('Old Qty'), 40),
                'image' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Form\Field::NAME,
                                'formElement' => Form\Element\Input::NAME,
//                            'elementTmpl' => 'ui/dynamic-rows/cells/thumbnail',
                                'elementTmpl' => 'Magestore_InventorySuccess/dynamic-rows/cells/thumbnail',
                                'dataType' => Form\Element\DataType\Media::NAME,
                                'dataScope' => 'image',
                                'fit' => __('Thumbnail'),
                                'label' => __('Thumbnail'),
                                'sortOrder' => 45,
                                'visible' => $this->getVisibleImage()
                            ],
                        ],
                    ],
                ],
                'serial_string' => $this->getTextColumn('serial_string', false, __('Serial Number'), 47),
                'add_serial_button' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Serial Action'),
                                'formElement' => 'container',
                                'componentType' => 'container',
                                'component' => 'Magestore_SerialSuccess/js/form/components/add-serial-button',
                                'actions' => [],
                                'title' => __('Add serial, 0 added'),
                                'dataScope' => 'id',
                                'sampleValue' => 'ahihi',
                                'provider' => null,
                                'visible' => $showAddSerial
                            ],
                        ],
                    ],
                ],
            ],
            $additionalColumns
        );
        $modifierColumns = array_replace_recursive(
            $modifierColumns,
            $this->getActionColumns()
        );
        return $modifierColumns;
    }
}
