<?php

namespace Magestore\SerialSuccess\Block\Adminhtml\SerialNumber;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    protected $_template = 'Magestore_SerialSuccess::serialNumber/add-serial-modal.phtml';

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $formFactory, $data);
    }
}