<?php
/**
 * Copyright © 2016 Magestore. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magestore\SerialSuccess\Controller\Adminhtml\InventorySuccess\TransferStock\External;

use Magestore\InventorySuccess\Model\TransferStock as TransferStockModel;
use Magestore\InventorySuccess\Api\Data\TransferStock\TransferStockInterface;

class Save extends \Magestore\InventorySuccess\Controller\Adminhtml\TransferStock\External\Save
{
    /**
     * save send stock products
     * if admin click on "start request stock" ($update_stock=true) then process update stock
     * @param $data
     * @param $update_stock
     */
    public function saveTransferStockProduct($data, $update_stock)
    {
        /** @var \Magestore\InventorySuccess\Model\TransferStock\TransferStockManagement $transferStockManagement */
        $transferStockManagement = $this->_objectManager->create('\Magestore\InventorySuccess\Model\TransferStock\TransferStockManagement');
        $sendProductsData = $data;
        $data = $this->reformatPostData($data);
        $id = $this->getRequest()->getParam('id');
        $transferStock = $this->_objectManager->create('\Magestore\InventorySuccess\Model\TransferStock');
        $transferStock->load($id);

        $transferStockManagement->saveTransferStockProduct($id, $data);
        if ($update_stock) {
            $transferStockManagement->updateStock($transferStock);
        }

        $serialSuccessEnabled = $this->_objectManager->create('Magento\Framework\Module\Manager')
            ->isEnabled('Magestore_SerialSuccess');
        if ($serialSuccessEnabled) {
            $serialItemFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ItemFactory');
            $serialTransFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\TransactionFactory');
            $collectionFactory = $this->_objectManager->create('Magestore\SerialSuccess\Model\ResourceModel\Item\CollectionFactory');

            foreach ($sendProductsData as &$p) {
                if($p['serial-data']){
                    $serials = $collectionFactory->create()
                        ->addFieldToSelect('serial')
                        ->addFieldToFilter('product_id', ['eq' => $p['id']])
                        ->getColumnValues('serial');

//                    $serials = array_diff($p['serial-data'], $serials);

                    foreach ($p['serial-data'] as $serial) {
                        if(!in_array($serial, $serials)){
                            $item = $serialItemFactory->create();
                            $item->setData('product_id', $p['id']);
                            $item->setData('serial', $serial);
                            $item->setData('created_at', time());
                            $item->setData('status', 1);
                            $item->save();
                        }else{
                            $item = $collectionFactory->create()
                                ->addFieldToFilter('serial', ['eq' => $serial])
                                ->getFirstItem();
                        }

                        $type = TransferStockModel::TYPE_TO_EXTERNAL;
                        if ($this->getRequest()->getParam('type')) {
                            $type = $this->getRequest()->getParam('type');
                        }
                        if($type == TransferStockModel::TYPE_TO_EXTERNAL){
                            $desc = json_encode(["transfer_to_external" => $id]);
                            $status = 2;
                        }else{
                            $desc = json_encode(["transfer_from_external" => $id]);
                            $status = 1;
                        }

                        $trans = $serialTransFactory->create();
                        $trans->setData('serial_item_id', $item->getId());
                        $trans->setData('status', $status);
                        $trans->setData('created_at', time());
                        $trans->setData('desc', $desc);
                        $trans->save();
                    }
                }
            }
        }
    }

    public function reformatPostData($data)
    {
        $id = $this->getRequest()->getParam('id');
        $newData = [];

        foreach ($data as $index => $value) {
            $item = [];
            $item['transferstock_id'] = $id;
            $item['product_id'] = $value['id'];
            $item['product_name'] = $value['name'];
            $item['product_sku'] = $value['sku'];
            $item['qty'] = $value['qty'];
            if(isset($value['serial_string']) && $value['serial_string']){
                $item['serial_string'] = $value['serial_string'];
            }

            $newData[$value['id']] = $item;
        }
        return $newData;
    }
}