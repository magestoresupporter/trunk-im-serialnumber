<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magestore\SerialSuccess\Ui\Component\Listing\Column\Option;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Options
 */
class TransactionStatus implements OptionSourceInterface
{

    /**
     * @var array
     */
    protected $options;

    /**
     * @var array
     */
    protected $currentOptions = [];

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray() {
        if ($this->options !== null) {
            return $this->options;
        }

        $this->generateCurrentOptions();

        $this->options = array_values($this->currentOptions);

        return $this->options;
    }

    /**
     * Generate current options
     *
     * @return void
     */
    protected function generateCurrentOptions() {
        $listOption = [
            '1' => 'Stock in',
            '2' => 'Stock out',
        ];
        foreach ($listOption as $key => $value) {
            $this->currentOptions[$key]['label'] = $value;
            $this->currentOptions[$key]['value'] = $key;
        }
    }
}
