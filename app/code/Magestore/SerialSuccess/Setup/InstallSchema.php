<?php
namespace Magestore\SerialSuccess\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        $setup->getConnection()->dropTable($setup->getTable('os_serial_items'));
        $setup->getConnection()->dropTable($setup->getTable('os_serial_transactions'));

        $table = $setup->getConnection()->newTable($setup->getTable('os_serial_items'))
                       ->addColumn(
                           'serial_item_id',
                           \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                           null,
                           [
                               'identity' => true,
                               'unsigned' => true,
                               'nullable' => false,
                               'primary' => true,
                           ],
                           'Serial item id'
                       ) ->addColumn(
                            'product_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            array(
                                'unsigned' => true,
                                'nullable' => false
                            ),
                            'Product id'
                        )->addColumn(
                            'serial',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            50,
                            array(),
                            'Serial content'
                        )->addColumn(
                            'created_at',
                            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                            null,
                            array(),
                            'created_at'
                        )->addColumn(
                            'status',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            50,
                            array(),
                            'status'
                        );
        $tranTable = $setup->getConnection()->newTable($setup->getTable('os_serial_transactions'))
                       ->addColumn(
                           'serial_trans_id',
                           \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                           null,
                           [
                               'identity' => true,
                               'unsigned' => true,
                               'nullable' => false,
                               'primary' => true,
                           ],
                           'Transaction id'
                        )->addColumn(
                            'serial_item_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            10,
                            array(),
                            'Serial id'
                        )->addColumn(
                            'created_at',
                            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                            null,
                            array(),
                            'created_at'
                        )->addColumn(
                            'status',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            50,
                            array(),
                            'Status'
                        )->addColumn(
                            'desc',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            250,
                            array(),
                            'Description'
                        );
        $setup->getConnection()->createTable($table);
        $setup->getConnection()->createTable($tranTable);

        $setup->getConnection()->addColumn(
            $setup->getTable('os_transferstock_product'),
            'serial_string',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial Number'
            )
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('os_transferstock_product'),
            'serial_shortfall',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial Number Shortfall'
            )
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('os_adjuststock_product'),
            'serial_string',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial Number'
            )
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('os_stocktaking_product'),
            'serial_string',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial Number'
            )
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('os_transferstock_activity_product'),
            'serial_string',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial Number'
            )
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'serial_string',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial Value'
            )
        );
        /* Add "barcode_value" into "sales_order_item" table */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_item'),
            'serial_string',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial Value'
            )
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_item'),
            'serial_shipping',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial Shipping'
            )
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_item'),
            'serial_refund',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial Refund'
            )
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_creditmemo_item'),
            'serial_string',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial string'
            )
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_shipment_item'),
            'serial_string',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial string'
            )
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_shipment'),
            'serial_string',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial string'
            )
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_shipment_grid'),
            'serial_string',
            array(
                'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable'  => true,
                'length'    => null,
                'default'   => null,
                'comment'   => 'Serial string'
            )
        );

        $setup->endSetup();
    }
}