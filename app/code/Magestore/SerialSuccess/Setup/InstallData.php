<?php
namespace Magestore\SerialSuccess\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    public function __construct
    (
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
    )
    {
        $configWriter->save('barcodesuccess/general/one_barcode_per_sku', 0);
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();


        $setup->endSetup();
    }
}