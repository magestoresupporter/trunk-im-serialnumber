<?php
namespace Magestore\SerialSuccess\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\Component\Form;
use Magento\Ui\Component\Container;

class Serial extends AbstractModifier {

    protected $_scopeName = 'product_form.product_form';
    protected $_groupContainer = 'os_product_serial';
    protected $_groupLabel = 'Serial Number';
    protected $_sortOrder = 16;

    protected $children = [
        'product_serial_listing' => 'os_product_serial_listing',
        'product_serial_container' => 'product_serial_container',
        'product_buttons' => 'product_buttons',
        'add_serial_modal' => 'add_serial_modal'
    ];

    /** @var \Magento\Framework\UrlInterface  */
    protected $urlBuilder;


    public function __construct (
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Backend\Model\Session $session
    ) {
        $session->setProductId($request->getParam('id'));
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * @param array $data
     * @return array
     */
    public function modifyData(array $data) {
        return $data;
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta) {
        $meta = array_replace_recursive(
            $meta,
            [
                $this->_groupContainer => [
                    'children' => $this->getChildren(),
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __($this->_groupLabel),
                                'component' => 'Magestore_SerialSuccess/js/form/components/fieldset',
                                'collapsible' => true,
                                'dataScope' => 'data',
                                'visible' => true,
                                'opened' => false,
                                'componentType' => \Magento\Ui\Component\Form\Fieldset::NAME,
                                'sortOrder' => $this->_sortOrder,
                                'actions' => [
                                    [
                                        'targetName' => $this->_scopeName . '.' . $this->_groupContainer . '.' .
                                            $this->children['product_serial_container'],
                                        'actionName' => 'render',
                                    ]
                                ]
                            ],
                        ],
                    ],
                ],
            ]
        );
        return $meta;
    }

    protected function getChildren() {
        $children = [
            $this->children['add_serial_modal'] => $this->getAddSerialModal(),
            $this->children['product_buttons'] => $this->getButtons(),
            $this->children['product_serial_container'] => $this->getProductSerialContainer()
        ];
        return $children;
    }

    protected function getProductSerialContainer() {
        $dataScope = 'product_serial_listing';
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'component' => 'Magestore_SerialSuccess/js/form/components/insert-listing',
                        'autoRender' => false,
                        'componentType' => 'insertListing',
                        'dataScope' => $this->children[$dataScope],
                        'externalProvider' => $this->children[$dataScope]. '.' . $this->children[$dataScope]
                            . '_data_source',
                        'ns' => $this->children[$dataScope],
                        'render_url' => $this->urlBuilder->getUrl('mui/index/render'),
                        'realTimeLink' => true,
                        'dataLinks' => [
                            'imports' => false,
                            'exports' => true
                        ],
                        'behaviourType' => 'simple',
                        'externalFilterMode' => true,
                        'imports' => [
                            'product_id' => '${ $.provider }:data.product_id'
                        ],
                        'exports' => [
                            'product_id' => '${ $.externalProvider }:params.product_id'
                        ],
                        'selectionsProvider' =>
                            $this->children[$dataScope]
                            . '.' . $this->children[$dataScope]
                            . '.product_serial_template_columns.ids',
                    ]
                ]
            ]
        ];
    }

    protected function getButtons() {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => Container::NAME,
                        'componentType' => Container::NAME,
                        'label' => false,
                        'template' => 'ui/form/components/complex',
                    ],
                ],
            ],
            'children' => [
                'add_serial' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'additionalClasses' => 'action-primary',
                                'formElement' => Container::NAME,
                                'componentType' => Container::NAME,
                                'component' => 'Magestore_SerialSuccess/js/form/components/product-add-serial-button',
                                'title' => __('Add Serial'),
                                'actions' => [
                                ]
                            ],
                        ],
                    ],
                ]
            ]
        ];
    }

    protected function getAddSerialModal() {
        $container = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => Container::NAME,
                        'componentType' => Container::NAME,
                        'sortOrder' => 10,

                    ],
                ],
            ],
            'children' => [
                'html_content' => [
                    'arguments' => [
                        'data' => [
                            'type' => 'html_content',
                            'name' => 'html_content',
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/form/components/html',
                                'content' => \Magento\Framework\App\ObjectManager::getInstance()
                                    ->create('Magestore\SerialSuccess\Block\Adminhtml\Product\Edit\Tab\Serial')
                                    ->toHtml()
                            ]
                        ]
                    ]
                ]
            ]
        ];
        return $container;
    }
}